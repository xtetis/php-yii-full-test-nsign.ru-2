<?php

namespace app\controllers;

use app\models\formatters\Formatter;
use app\models\Phones;
use app\models\Products;
use app\models\Loans;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * Отображает отформатированный номер телефона
     *
     * @return string
     */
    public function actionPhoneformat()
    {
        $model__phones = Phones::find()->all();
        if ($model__phones)
        {
            foreach ($model__phones as $model__phone)
            {
                $mask = '9 (999) 999 - 99 - 99';
                echo 'Phone = ' . $model__phone->phone . ';' .
                ' mask=' . $mask . ';' .
                ' masked = ' . Yii::$app->formatter->asPhoneMask(
                    $model__phone->phone,
                    $mask
                ) . '<br>';
            }
        }
    }

    /**
     * Отображает GridView c данными из XML
     *
     * @return string
     */
    /**
     * @param $item
     */
    public function actionXmlgridview()
    {
        $products = Products::getProductsArray();

        $searchAttributes = [
            'id'            => 'ID',
            'categoryId'    => 'ID категории',
            'price'         => 'Цена',
            'hidden'        => 'Скрыто',
            'category_name' => 'Имя категории'];
        $searchModel   = [];
        $searchColumns = [];

        foreach ($searchAttributes as $searchAttribute => $searchLabel)
        {
            $filterName                    = 'filter' . $searchAttribute;
            $filterValue                   = Yii::$app->request->getQueryParam($filterName, '');
            $searchModel[$searchAttribute] = $filterValue;
            $searchColumns[]               = [
                'attribute' => $searchAttribute,
                'filter'    => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                'value'     => $searchAttribute,
                'header'    => $searchLabel,
            ];
            $products = array_filter($products, function ($item) use (&$filterValue, &$searchAttribute)
            {
                return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
            });
        }

        $dp_products = new ArrayDataProvider([
            'allModels'  => $products,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort'       => [
                'attributes' => $searchAttributes,
            ],
        ]);

        return $this->render('view_xmlgridview',
            [
                'dp_products'      => $dp_products,
                'searchModel'      => $searchModel,
                'searchColumns'    => $searchColumns,
                'labelsAttributes' => $labelsAttributes,
            ]);

    }


    /**
     * 
     *
     * @return string
     */
    public function actionShowloans()
    {
        $model_Loans = Loans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $model_Loans, 
        ]);

        return $this->render('view_loans',
            [
                'dataProvider'      => $dataProvider,
            ]);
    }


    /**
     * 
     *
     * @return string
     */
    public function actionHelpers()
    {
        $anytext = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras faucibus lacus nec justo tempus feugiat. Fusce pretium, neque volutpat gravida porttitor, lectus dui porttitor quam, sed scelerisque lacus est eu.';

        return $this->render('view_helpers',
            [
                'anytext'      => $anytext,
            ]);
    }




}
