<?php

use yii\helpers\BaseStringHelper;
use yii\helpers\Inflector;

echo '<b>Пункт 1</b><br>';

echo '<b>Изначальный текст</b>:'.$anytext.'<br>';
echo '<b>Количество слов в изначальном тексте</b>:'.BaseStringHelper::countWords($anytext).'<br><br>';
echo '<b>Урезанный до 12 слов текст</b>:'.BaseStringHelper::truncateWords($anytext,12).'<br>';
echo '<b>Количество слов в урезанном тексте</b>:'.BaseStringHelper::countWords(BaseStringHelper::truncateWords($anytext,12)).'<br><br>';




echo ' <br><br><b>Пункт 2</b><br>';
$created_at = 'created_at';
echo '<b>Изначальный текст</b>:'.$created_at.'<br>';
echo '<b>Отформатированный текст</b>:'.implode('',explode(' ',BaseStringHelper::mb_ucwords(implode(' ',BaseStringHelper::explode($created_at,'_'))
))).'<br>';




echo '<br><br><b>Пункт 3</b><br>';
$start_text = 'Купи слона';
echo '<b>Изначальный текст</b>:'.$start_text.'<br>';
echo '<b>транслитерированный текст</b>:'.Inflector::transliterate($start_text).'<br>';
	