<?php

    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */
    /* @var $model app\models\ContactForm */

    use yii\grid\GridView;

    $this->title = 'Contact';
?>


<?php
echo GridView::widget([
    'dataProvider' => $dp_products,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered',
    ],
    'filterModel'  => $searchModel,
    'columns'      => $searchColumns,
]);
