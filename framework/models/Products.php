<?php

namespace app\models;

use Yii;
use yii\base\Model;


class Products extends Model
{
 
    public function getProductsArray()
    {
        $categories = [];
        $xml_file   = __DIR__ . '/../xml/categories.xml';
        if (file_exists($xml_file))
        {
            $xml        = simplexml_load_string(file_get_contents($xml_file));
            $categories = json_decode(json_encode($xml), true)['item'];

        }

        $products = [];
        $xml_file = __DIR__ . '/../xml/products.xml';
        if (file_exists($xml_file))
        {
            $xml      = simplexml_load_string(file_get_contents($xml_file));
            $products = json_decode(json_encode($xml), true)['item'];

        }

        foreach ($products as &$product)
        {
            $find_category = false;
            foreach ($categories as $category)
            {
                if ($category['id'] == $product['categoryId'])
                {
                    $product['category_name'] = $category['name'];
                    $find_category            = true;
                }
            }
            if (!$find_category)
            {
                $product['category_name'] = 'NONE';
            }
        }

        return $products;
    }

}
