<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "loans".
 *
 * @property string $id Первичный ключ
 * @property double $summ Сумма займа
 * @property string $purpose Цель займа
 * @property string $create_date Дата создания
 */
class Loans extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['summ', 'purpose'], 'required'],
            [['summ'], 'number'],
            [['create_date'], 'safe'],
            [['purpose'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'summ' => 'Сумма займа',
            'purpose' => 'Цель займа',
            'create_date' => 'Дата создания',
        ];
    }
}
