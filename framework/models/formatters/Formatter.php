<?php

namespace app\models\formatters;

class Formatter extends \yii\i18n\Formatter
{
    /**
     * Преобразовыввает номер телефона по маске
     * Маска в виде "+9 (999) 999 - 99 - 99"
     *
     * @param  string   $phone
     * @param  $mask
     * @return String
     */
    public static function asPhoneMask(
        $phone,
        $mask
    )
    {
        $phone_digit = Formatter::asPhoneNumeric($phone);
        $mask_array  = explode('9', $mask);

        if (strlen($phone_digit) != (count($mask_array) - 1))
        {
            return $phone;
        }

        $phone_result_array = [];
        $i                  = 0;
        foreach ($mask_array as $mask_item)
        {
            $phone_result_array[] = $mask_item;
            if (count($mask_array) == ($i - 1))
            {
                break;
            }
            $phone_result_array[] = $phone_digit[$i];
            $i++;
        }
        $phone = implode('', $phone_result_array);

        return $phone;
    }

    /**
     * Преобразовывает телефон в набор цифр
     *
     * @param  string   $phone
     * @return String
     */
    public static function asPhoneNumeric($phone)
    {
        $re    = '/\D+/m';
        $phone = preg_replace($re, '', $phone);

        return $phone;
    }
}
