##ОТОБРАЖЕНИЕ ДАННЫХ

**Напишите свой formatter телефонного номера , формат/маска задается опционально. В базе лежит числовое представление номера.**

Сам класс форматтера находится в 
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/models/formatters/Formatter.php 

В контроллере Site
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/controllers/SiteController.php
actionPhoneformat() Отображает отформатированный номер телефона.
Телефоны берутся из базы. Для этого используется модель
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/models/Phones.php 


---


**Вывести список продуктов и связанных с ними категорий используя GridView, с возможностью сортировать, фильтровать. Из xml https://drive.google.com/open?id=1l_idMT9M-UfBD99AHoxRhwADbmJi9oOm **

Сами продукты формируются в модели 
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/models/Products.php 

В контроллере Site
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/controllers/SiteController.php

 actionXmlgridview() происходит создание датапроdайдера и поиск

Отображение в 
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/views/site/view_xmlgridview.php 


---


##ВИДЖЕТЫ

**Есть проект который разрабатывался длительное время, с большим количеством GridView. Теперь клиент захотел что бы везде в списках ( GridView ) где выводиться "(не задано)" если column возвращает null , выводилась фраза "[нет данных]". Что вы сделаете ? Укажите ссылку на решение. **

VIEW в 
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/views/site/view_loans.php 
И там используется GridView. В котором не указан явно formatter

В конфиге
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/config/web.php 

Глобально указывается 
```php
    'formatter' => [
        'class' => 'yii\i18n\Formatter',
        'nullDisplay' => '[нет данных]',
    ],
```
но в соем случае Formatter дополнен функционалом (из предыдущей задачи). 
Потому конфиг выглядит так 
```php
        'formatter' => [
            'class' => 'app\models\formatters\Formatter',
            'nullDisplay' => '[нет данных]',
        ],
```

В кажечтве данных для отображения используется таблица loans. И соответственно модель
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/models/Loans.php 

и в контроллере Site

https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/controllers/SiteController.php

происходит формирование dataProvider


---

##ХЕЛПЕРЫ

**Вам нужно обрезать строку, по определенному количеству слов. Что бы из 30 слов, выводилось 12. Приложите ссылку на код. **

VIEW в 
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/views/site/view_helpers.php


и в контроллере Site (actionHelpers)

https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/controllers/SiteController.php

происходит формирование изначального текста в 30 слов

```php
BaseStringHelper::truncateWords($anytext,12)
```

---

**Вам нужно преобразовать строку из created_at в CreatedAt. Приложите ссылку на код. **

VIEW в 
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/views/site/view_helpers.php


```php
implode('',explode(' ',BaseStringHelper::mb_ucwords(implode(' ',BaseStringHelper::explode($created_at,'_'))
)))
```

---

**Вам нужно строку "Купи слона" преобразовать в "Kupi slona". Приложите ссылку на код. **

VIEW в 
https://bitbucket.org/xtetis/test-nsign-ru-2/src/master/framework/views/site/view_helpers.php

```php
Inflector::transliterate($start_text)
```